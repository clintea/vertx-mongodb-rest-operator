#!/bin/bash
kubectl delete -f deploy/crds/lab_v1_vertxmongodbrest_cr.yaml
kubectl delete -f deploy/operator.yaml
kubectl delete -f deploy/role_binding.yaml
kubectl delete -f deploy/role.yaml
kubectl delete -f deploy/service_account.yaml
kubectl delete -f deploy/crds/lab_v1_vertxmongodbrest_crd.yaml
kubectl delete -f deploy/olm-catalog/vertx-mongodb-rest-operator/0.0.3/vertx-mongodb-rest-operator.v0.0.3.clusterserviceversion.yaml
